---
title: "Facebook"
---

## Group

A place for the community to share thoughts, ideas, inspiration, all that stuff.

The Facebook group page is private, and I vet everyone who asks to join. Generally if you are a Patreon member I'll accept you, or a friend of a friend, or just somebody who I think is genuine about wanting to be a part of a friendly community.

Group link: https://www.facebook.com/groups/guitarbytheglass/
