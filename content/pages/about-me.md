---
title: "About Me"
---

## Guitar by the Glass

This is an outlet for me. It's my pet project. I have an idea of how I think learning an instrument as an adult should be, and each week I do my best to provide that experience to as many as I can, given the limited time I have.

## Work

My brothers and I have our own business that makes cloud software for breweries. It's called [*BrewKeeper*](https://www.brewkeeper.com.au), and at its most basic level it manages inventory. I work full time on that, and we're hoping eventually we will all be able to make it our full time work.



## Study

I recently completed my *Masters of Information Technology* at UWA.

I have a bachelors in Primary Education. Before working full time on my own business, I was a Primary School teacher for 5 years.

