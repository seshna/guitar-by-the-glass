---
title: "Book 4 songs"
---

To sing along, google the lyrics

To request a song, politely call out the page number in the parenthesis for the players

- **A Horse With No Name** by America (Book 4 pg. 4)
- **Amie** by Pure Prairie Leage (Book 4 pg. 6)
- **And The Band Played Waltzing Matilda** by The Pogues (Book 4 pg. 8)
- **Bad Things** by Jace Everett (Book 4 pg. 10)
- **Come Back Again** by Daddy Cool (Book 4 pg. 11)
- **Fairytale Of New York** by The Pogues (Book 4 pg. 12)
- **Friday I’m In Love** by The Cure (Book 4 pg. 14)
- **House Of The Rising Sun** by The Animals (Book 4 pg. 15)
- **How To Make Gravy** by Paul Kelly (Book 4 pg. 16)
- **I Got You** by Split Enz (Book 4 pg. 18)
- **I’m Yours** by Jason Mraz (Book 4 pg. 20)
- **I Was Only 19** by Redgum (Book 4 pg. 22)
- **Johnny B. Goode** by Chuck Berry (Book 4 pg. 24)
- **Linger** by The Cranberries (Book 4 pg.25)
- **Little Talks** by Of Monsters And Men (Book 4 pg. 26)
- **Losing My Religion** by R.E.M (Book 4 pg. 28)
- **Mr. Jones** by Counting Crows (Book 4 pg. 30)
- **Make Me Smile (Come Up And See Me)** by Steve Harley (Book 4 pg. 32)
- **Needles And Pins** by The Searchers (Book 4 pg. 33)
- **One** by U2 (Book 4 pg. 34)
- **Raindrops Keep Falling On My Head** by BJ Thomas (Book 4 pg. 35)
- **Sloop John B** by Beach Boys (Book 4 pg. 36)
- **Summer of 69** by Brian Adams (Book 4 pg. 37)
- **Sweet Caroline** by Neil Diamond (Book 4 pg. 38)
- **The Needle And The Damage Done** by Neil Young (Book 4 pg. 39)
- **The Way** by Fastball (Book 4 pg. 40)
- **The Wanderer** by Dion (Book 4 pg. 42)
- **Three Little Birds** by Bob Marley (Book 4 pg. 43)
- **Valerie** by Amy Winehouse (Book 4 pg. 44)
- **When Love Comes To Town** by B.B King feat. U2 (Book 4 pg. 46)
