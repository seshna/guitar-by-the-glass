---
title: "Book 5 songs"
---

To sing along, google the lyrics

To request a song, politely call out the page number in the parenthesis for the players

- **Ain’t No Sunshine When She’s Gone** by Bill Withers (Book 5 pg. 4)
- **Blueberry Hill** by Fats Domino (Book 5 pg. 5)
- **Breakdown** by Tom Petty (Book 5 pg. 6)
- **Cocaine** by Eric Clapton (Book 5 pg. 8)
- **Diamonds On The Inside** by Ben Harper (Book 5 pg. 9)
- **End Of The Line** by Traveling Wilburys (Book 5 pg. 10)
- **Flame Trees** by Cold Chisel (Book 5 pg. 12)
- **Galway Girl** by Steve Earle (Book 5 pg. 14)
- **Ghost Riders In The Sky** by Johnny Cash (Book 5 pg. 16)
- **Heart Of Stone** by The Rolling Stones (Book 5 pg. 17)
- **Hotel California** by The Eagles (Book 5 pg. 18)
- **It Hurts Me Too** by Elmore James (Book 5 pg. 20)
- **Knockin’ On Heaven’s Door** by Bob Dylan (Book 5 pg. 21)
- **Like A Rolling Stone** by Bob Dylan (Book 5 pg. 22)
- **Live Forever** by Oasis (Book 5 pg. 24)
- **Lola** by The Kinks (Book 5 pg. 26)
- **Looking Out My Back Door** by CCR (Book 5 pg. 28)
- **Lonely Boy** by Paul Anka (Book 5 pg. 29)
- **Love Cats** by The Cure (Book 5 pg. 30)
- **MMMM MMMM MMMM MMMM** by Crash Test Dummies (Book 5 pg. 32)
- **Mustang Sally** by Wilsen Pickett (Book 5 pg. 33)
- **My Island Home** by Warumpi Band (Book 5 pg. 34)
- **Nobody Knows You When You’re Down And Out** by Eric Clapton (Book 5 pg. 36)
- **On The Road Again** by Canned Heat (Book 5 pg. 38)
- **Pretty Flamingo** by Manfred Mann (Book 5 pg. 40)
- **Promises** by Eric Clapton (Book 5 pg. 41)
- **Somebody’s Crying** by Chris Isaak (Book 5 pg. 42)
- **Robin Hood Songs** by Roger Miller (Book 5 pg. 38)
- **Sweet Home Alabama** by Lynyrd Skynyrd (Book 5 pg. 44)
- **The Last Time** by The Rolling Stones (Book 5 pg. 46)
- **Times Like These** by Foo Fighters (Book 5 pg. 47)
- **Will Ye Go Lassie Go** by The Corries (Book 5 pg. - 
