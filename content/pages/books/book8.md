---
title: "Book 80's songs"
---

To sing along, google the lyrics

To request a song, politely call out the page number in the parenthesis for the players

- **Africa** by Toto (Book 80's pg. 4)
- **And We Danced** by The Hooters  (Book 80's pg. 6)
- **Boys Light Up** by Australian Crawl (Book 80's pg. 8)
- **Boys Of Summer** by Don Henley (Book 80's pg. 10)
- **Come On Eileen** by Dexy’s Midnight Runners (Book 80's pg. 12)
- **Dancing In The Dark** by Bruce Springsteen (Book 80's pg. 14)
- **Don’t You (Forget About Me)** by Simple Minds (Book 80's pg. 16)
- **Every Rose Has It’s Thorn** by Poison  (Book 80's pg. 18)
- **Everybody Wants To Rule The World** by Tears For Fears (Book 80's pg. 20)
- **Fire** by Bruce Springsteen (Book 80's pg. 22)
- **Hold Me Now** by Thompson Twins (Book 80's pg. 23)
- **I Got My Mind Set On You** by George Harrison (Book 80's pg. 24)
- **I Want To Know What Love Is** by Foreigner (Book 80's pg. 26)
- **I Won’t Back Down** by Tom Petty And The Heartbreakers  (Book 80's pg. 27)
- **Jokerman** by Bob Dylan (Book 80's pg. 28)
- **Karma Chameleon** by Culture Club (Book 80's pg. 30)
- **Live It Up** by Mental As Anything (Book 80's pg. 32)
- **Livin’ On A Prayer** by Bon Jovi (Book 80's pg. 34)
- **Made Of Stone** by The Stone Roses (Book 80's pg. 36)
- **Manic Monday** by The Bangles (Book 80's pg. 38)
- **Mystify** by INXS (Book 80's pg. 40)
- **Never Gonna Give You Up** by Rick Astley (Book 80's pg. 41)
- **Never Tear Us Apart** by INXS (Book 80's pg. 42)
- **Red Red Wine** by UB40 (Book 80's pg. 43)
- **Roam** by B52’s (Book 80's pg. 44)
- **Safety Dance** by Men Without Hats (Book 80's pg. 46)
- **Senses Working Overtime** by XTC (Book 80's pg. 48)
- **She’s Like The Wind** by Patrick Swayze (Book 80's pg. 50)
- **Solid Rock** by Goanna (Book 80's pg. 52)
- **Summer Rain** by Belinda Carlisle (Book 80's pg. 54)
- **Take On Me** by A-ha (Book 80's pg. 56)
- **Total Eclipse Of The Heart** by Bonnie Tyler (Book 80's pg. 58)
- **You Got Lucky** by Tom Petty And The Heartbreakers (Book 80's pg. 60)
