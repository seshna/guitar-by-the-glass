---
title: "Book 1 songs"
---

To sing along, google the lyrics

To request a song, politely call out the page number in the parenthesis for the players

- **3am** by Matchbox20 (Book 1 pg. 4)
- **Bad Moon Rising** by CCR (Book 1 pg. 8)
- **Brown Eyed Girl** by Van Morrison (Book 1 pg. 6)
- **Dock Of The Bay** by Otis Redding (Book 1 pg. 10)
- **Dumb Things** by Paul Kelly (Book 1 pg. 12)
- **Heart Of Gold** by Neil Young (Book 1 pg. 9)
- **Father and Son** by Cat Stevens (Book 1 pg. 14)
- **Folsom Prison Blues** by Johnny Cash (Book 1 pg. 16)
- **Hallelujah** by Leonard Cohen (Book 1 pg. 18)
- **Handle With Care** by Traveling Wilburys (Book 1 pg. 20)
- **Have You Ever Seen The Rain?** by CCR (Book 1 pg. 17)
- **I Walk The Line** by Johnny Cash (Book 1 pg. 42)
- **I’m A Believer** by The Monkees (Book 1 pg. 24)
- **Lay Down Sally** by Eric Clapton (Book 1 pg. 22)
- **Lean On Me** by Bill Withers (Book 1 pg. 26)
- **Moondance** by Van Morrison (Book 1 pg. 28)
- **Opportunity** by Pete Murray (Book 1 pg. 30)
- **Reckless** by Australian Crawl (Book 1 pg. 32)
- **Runaway Train** by Soul Asylum (Book 1 pg. 34)
- **Save Tonight** by Eagle Cherry (Book 1 pg. 23)
- **Tainted Love** by Soft Cell (Book 1 pg. 36)
- **Wagon Wheel** by Old Crow Medicine Show (Book 1 pg. 38)
- **Wonderwall** by Oasis (Book 1 pg. 40)
