---
title: "Book 2 songs"
---

To sing along, google the lyrics

To request a song, politely call out the page number in the parenthesis for the players

- **I’ll Fly Away** (Book 2 pg. 5)
- **Man Of Constant Sorrow** (Book 2 pg. 6)
- **Another Brick In The Wall, Pt. 2** by Pink Floyd (Book 2 pg. 7)
- **Before Too Long** by Paul Kelly (Book 2 pg. 8)
- **Big Jet Plane** by Angus And Julia Stone (Book 2 pg. 9)
- **Better Days** by Pete Murray (Book 2 pg. 10)
- **Bizarre Love Triangle** by Frente (Book 2 pg. 12)
- **Comes A Time** by Neil Young (Book 2 pg. 13)
- **Down On The Corner** by Creedence Clearwater Revival (Book 2 pg. 14)
- **For What It’s Worth** by Buffalo Springfield (Book 2 pg. 15)
- **Dirty Old Town** by The Pogues  (Book 2 pg. 16)
- **Good Riddance** by Green Day (Book 2 pg. 18)
- **Having A Party** by Sam Cooke (Book 2 pg. 19)
- **Harvest Moon** by Neil Young (Book 2 pg. 20)
- **Here’s To Us** by Halestorm (Book 2 pg. 21)
- **King Of The Road** by Roger Miller (Book 2 pg. 22)
- **Learning To Fly** by Tom Petty (Book 2 pg. 23)
- **London Still** by The Waifs (Book 2 pg. 24)
- **Mrs Robinson** by Simon And Garfunkel (Book 2 pg. 26)
- **My Happiness by Powderfinger** (Book 2 pg. 28)
- **Nothing Compares To You** by Sinéad O’Connor(Book 2 pg. 30)
- **Ring Of Fire** by Johnny Cash (Book 2 pg. 31)
- **Riptide** by Vance Joy (Book 2 pg. 32)
- **Romeo And Juliet** by Dire Straits (Book 2 pg. 34)
- **Son Of A Preacher** by Dusty Springfield (Book 2 pg. 36)
- **Suzie Q** by Creedence Clearwater Revival (Book 2 pg. 37)
- **Take Me Home, Country Roads** by John Denver (Book 2 pg. 38)
- **Tell It To Me** by Old Crow Medicine Show (Book 2 pg. 39)
- **The Man Who Sold The World** by David Bowie (Book 2 pg. 40)
- **Thinking Out Loud** by Ed Sheeran (Book 2 pg. 42)
- **Time After Time** by Cyndi Lauper (Book 2 pg. 44)
- **Walkin Down The Road** by Ozark Mountain Daredevils (Book 2 pg. 46)
- **Werewolves Of London** by Warren Zevon (Book 2 pg. 47)
- **Wish You Were Here** by Pink Floyd (Book 2 pg. 48)
