---
title: "Patreon"
---

Link: [https://patreon.com/guitarbytheglass](https://patreon.com/guitarbytheglass)

I spend a lot of time making sure my chord charts are easy to read, easy to follow. If you want access to my dropbox where I keep them, just become a patreon at any tier level.

Here is the structure of the dropbox folder
{{< fileTree >}}
* Create a chart
    * chord charts / chord chart images
    * strumming patterns / strumming pattern images
    * 01 Song Chart Template.dotx / make your own!
* GbtG Books
    * GbtG Book 1 songs / docx and pdfs of each song
    * GbtG Book 2 songs / ""
    * GbtG Book 3 songs
    * ... etc
* GbtG Floaters / New songs each week
* GbtG Floaters new / Upcoming songs
* Song lists / Lists grouped by category
* Uke Books / Ukulele chord chart books
* Uke Floaters / Additional uke chord charts
{{< /fileTree >}}

## My chord conventions
I like to make sure my chord charts are easy to follow, and I've found that to accomplish that, the normal chord chart conventions aren't expressive enough. Below are all of the conventions I use in my charts.
![Chord Conventions](/images/chord-conventions.png)

eg.\
<code><span style='color: red'>[A]</span>Hey now, <span style='color: red'>[/]</span>look at that cow</code>

- Strum the <code><span style='color: red'>[A]</span></code> chord just before singing <code>"Hey"</code>
- Strum the <code><span style='color: red'>[A]</span></code> chord again just before singing <code>"look"</code>
