---
title: "Home"
---

> Are you watching a session? Want to sing along or request a song? Ask which book we're playing from and click [here]({{< ref "sing-along" >}} "Guitar by the Glass books")

I just needed a landing page that is a central link to all my other sites, that also includes some basic information how I tie it all together.

My name is Shaun, I currently live in Kalamunda, Western Australia.

{{< figureCupper
img="profile.jpg"
caption="Shaun likes to learn just enough of each instrument to be mediocre, then more onto the next."
command="Resize"
options="200x" >}}
